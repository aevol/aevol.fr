---
title: "Publications"
date: 2023-03-31T14:15:19+02:00
draft: false
---

## Main Publications
- Banse, Luiselli et al., Molecular Ecology, 2023  
    Forward-in-time simulation of chromosomal rearrangements: The invisible backbone that sustains long-term adaptation
- Liard et al., Int. Conf. Artif. Life, 2018 \<Best Paper Award\>  
    The Complexity Ratchet: Stronger than selection, weaker than robustness
- Frenoy et al., Plos Comp. Biol., 2013  
    Genetic Architecture Promotes the Evolution and Maintenance of Cooperation
- Batut et al., BMC Bioinfo., 2013  
    In silico experimental evolution: a tool to test evolutionary scenarios
- Misevic et al., Int. Conf. Artif. Life, 2012 \<Best Paper Award\>  
    Effects of public good properties on the evolution of cooperation
- Frenoy et al., Int. Conf. Artif. Life, 2012  
    Robustness and evolvability of cooperation
- Parsons, PhD Thesis, INSA-Lyon, 2011  
    Indirect Selection in Darwinian Evolution: Mechanisms and Implications
- Beslon et al., Biosystems, 2010  
    Scaling laws in bacterial genomes: A side-effect of selection of mutational robustness?
- Parsons et al., Int. Conf. Artif. Life, 2010  
    Importance of the Rearrangement Rates on the Organization of Genome Transcription
- Knibbe et al., Mol. Biol. Evol., 2007  
    A Long-Term Evolutionary Pressure on the Amount of Noncoding DNA
- Knibbe, PhD Thesis, INSA-Lyon, 2006  
    Structuration des génomes par sélection indirecte de la variabilité mutationnelle, une approche de modélisation et de simulation

## Other Publications

- Daudey et al., Int. Conf. Artif. Life, 2024  
    Aevol_4b: Bridging the gap between artificial life and bioinformatics
- Knibbe et al., Int. Conf. Artif. Life, 2014  
    What happened to my genes? Insights on gene family dynamics from digital genetics experiments
- Beslon et al., Europ. Conf. Artif. Life, 2013  
    An alife game to teach evolution of antibiotic resistance
- Misevic et al., Europ. Conf. Artif. Life, 2013  
    In silico evolution of transferable genetic elements
- Parsons et al., Int. Conf. Artif. Life, 2012  
    The Paradoxical Effects of Allelic Recombination on Fitness
- Hindre et al., Nat. Rev. Microbiol., 2012  
    New insights into bacterial adaptation through in vivo and in silico experimental evolution
- Parsons et al., Europ. Conf. Artif. Life, 2011  
    Homologous and Nonhomologous Rearrangements: Interactions and Effects on Evolvability
- Knibbe et al., Europ. Conf. Artif. Life, 2011  
    Parsimonious Modeling of Scaling Laws in Genomes and Transcriptomes
- Parsons et al., MajecSTIC, 2010  
    Aevol : un modèle individu-centré pour l’étude de la structuration des génomes
- Beslon et al., Intel. Data Anal. J., 2010  
    From digital genetics to knowledge discovery: Perspectives in genetic network understanding
- Sanchez-Dehesa, PhD Thesis, INSA-Lyon, 2009  
    RÆvol : un modèle de génétique digitale pour étudier l’évolution des réseaux de régulation génétiques
- Knibbe et al., Artif. Life, 2008  
    The Topology of the Protein Network Influences the Dynamics of Gene Order: From Systems Biology to a Systemic Understanding of Evolution
- Knibbe et al., J. Theor. Biol., 2007  
    Evolutionary coupling between the deleteriousness of gene mutations and the amount of non-coding sequences
- Knibbe et al., Europ. Conf. Artif. Life, 2005  
    Self-adaptation of genome size in artificial organisms
