---
title: "Contributors"
date: 2024-11-22T06:33:32+01:00
draft: false
---

## Main Contributors

- Guillaume Beslon
- Carole Knibbe
- David Parsons
- Jonathan Rouzaud-Cornabas
- Vincent Liard
- Bérénice Batut
- Yolanda Sanchez-Dehesa
- Nicolas Comte
- Marco Foley
- Juliette Luiselli
- Laurent Turpin
- Théotime Grohens
