This repository has moved...
============================

Please use the new location https://gitlab.com/aevol/aevol.fr

To update your git repo, use:

```
git remote set-url origin git@gitlab.com:aevol/aevol.fr.git
```
