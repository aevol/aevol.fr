Aevol website
=============

This repo holds the files used to generate the aevol website served at https://www.aevol.fr.

Contributing
------------

Want to add or modify a page, follow these steps:

### Fork the project

Go to https://gitlab.com/aevol/aevol.fr and click on the `Fork` button at the top-right corner.

Fill in the form with:

* Project name: aevol.fr is the natural choice but you may pick another name
* Project URL: we recommend you place the project in your personnal namespace
* Project slug:  aevol.fr is the natural choice but you may pick another name
* The Project description doesn't matter
* Visibility level: the project has to be public for the gitlab pages stuff to work

Finally, click on the  `Fork project` button

This creates your own **copy** of the project, including your own git repository, you can push to it regardless of your permissions on the original project a.k.a. *upstream*, you will then be able to submit your work to *upstream* as a Merge Request.


### Generate the view corresponding to your fork

Right after forking the project, there is no website that corresponds to your fork yet. If you want to check that everything is OK you can trigger site-generation by hand.

Go to your fork's gitlab page and select Build -> Pipelines in the menu. you can click on the "Run pipeline" button and again, on the next page "Run pipeline". This will trigger the CI (Continuous Integration) pipeline that will run on one of the workers. This pipeline generates the website and publish it on __your fork's gitlab-pages page__. To find out the URL of this page, select __Deploy -> Pages__ in the menu, you will find a link to your personal version of the aevol webpage there. It should be something like https://<your-gitlab-login>.gitlab.io/aevol.fr/

Looks ugly? That's normal. The baseURL variable is set to the "real" website (http://www.aevol.fr/). This will be the first change you'll have to make. We'll now take you through the process ;).


### Make your first change

1. Clone the repo

The repo you need to clone is **your fork's** git repo.

To make things clear we will name our remote "fork" rather than the default "origin" that may get misleading. You can do this with the --origin option:

```
git clone git@gitlab.com:<your-gitlab-login>/aevol.fr.git --origin fork
cd aevol.fr
```

2. Create a branch to work on

Let's create a branch ```fork-config``` where we will make our changes:

```
git switch -c fork-config
```

3. Make your change

Now you are ready to actually make your changes. Open the ```config.toml``` file and modify the value of the baseURL variable to reflect your fork's gitlab-pages page (https://<your-gitlab-login>.gitlab.io/aevol.fr/).

4. Commit and push your change

When you're done, commit your change (commit message may be "set baseURL to fork's gitlab-pages url") and push it to your fork:

```
git push fork fork-config
```

This will automatically trigger the CI pipeline on your fork (you can verify that by selecting Build -> Pipelines on your fork's gitlab page). This pipeline generates the website and deploys it to your fork's gitlab-pages page (this usually takes under a minute). It should now look like the official https://www.aevol.fr page.

### Get work done

Create a new branch for the changes you intend to make (step 2 of the previous section)

Repeat steps 3 and 4 of the previous section until you are happy with your changes and how they look on your fork's gitlab-pages page.

### Get your changes on the actual website (http://www.aevol.fr/)

When you're satisfied with your changes, you are ready to submit them as a Merge Request.

Well, actually not quite, you don't want to include ad-hoc configurations such as your modification to the baseURL or the ci configuration in your MR do you? Time to rebase your work.

#### Rebase your work

##### The easy way

If you don't care about what's about to happen simply type the following command and it will do what you need (rebase your work on main, after stripping it of the loacl-configuration commits):

```
git rebase fork-config --onto main
```

##### ... or the more involved way

If on the other hand you do want to understand what's going on in more detail, do the following instead:


If it's your first time doing an interactive rebase it might be safest to make sure you don't lose anything by creating a fallback branch:

```
git branch fallback
```

Now the rebase. You want your branch to have the upstream main branch as its new base.

```
git rebase -i origin/main
```

This will open your core editor with a script containing something like that:

```
pick f990a81 run CI regardless of the branch
pick 5d944d7 update baseURL
pick 0170e5f your neat modification
```

You want git to apply ("pick") the commit(s) where your actual modifications are but not the first two that correspond to your ad-hoc config. Simply edit this script by replacing "pick" with "drop" on the corresponding lines (you can also simply delete the lines), save and quit.

If you're confortable with git, you may have noticed that you could have achieved the same result with a plain old (not interactive) rebase with `git rebase 5d944d7 --onto origin/main`.

#### Submit a Merge Request

That's it, you have rebased your work to keep only the meat within it. You fisrt need to push it to your fork:

```
git push fork your-branch
```

Now it's time to do the MR.

Go to your fork's gitlab page (something like https://gitlab.com/your-login/aevol.fr), select the branch you have pushed your work to and click on the "Create merge request" button.

Give an explicit title to your MR and add a description for your reviewer to know what it is about. Make sure the destination branch is `aevol/aevol.fr:main` and click the "Create merge request" button.

That's it, now someone on the team will review your MR which will hopefully get integrated very soon.

Thank you for your contribution!
